extends KinematicBody2D

const UP = Vector2(0,-1)
const GRAVITY = 5
const SPEED = 50
const JUMP_HEIGHT = -125

var motion = Vector2()
var direction = 'right'
var state = 'idle_'

var action = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	animation_loop()

func _physics_process(delta):
	control_loop()
	
	motion.y += GRAVITY
	motion = move_and_slide(motion, UP)

func control_loop():
	if Input.is_action_pressed('ui_right'):
		motion.x = SPEED
		direction = 'right'
	elif Input.is_action_pressed('ui_left'):
		motion.x = -SPEED
		direction = 'left'
	else:
		motion.x = 0
		
	if is_on_floor() and Input.is_action_pressed('action1'):
		motion.y = JUMP_HEIGHT
	
	if Input.is_action_just_pressed('action2'):
		interact()
		# get_parent().activate_yoku()

func animation_loop():
	if motion.x != 0:
		state = 'walk_'
	else:
		state = 'idle_'
		
	anim_switch(str(state,direction))

func anim_switch(animation):
	var new_anim = animation
	if $anim.current_animation != new_anim:
		$anim.play(animation)

func interact():
	if action != null:
		if typeof(action) == TYPE_VECTOR2:
			go_to_destination()
		elif action.get('GROUP') == 'monitor':
			activate_monitor()
	else:
		pass

func go_to_destination():
	if action != null:
		position = action

func activate_monitor():
	if action.type == 1:
		action.activate()
	elif action.type == 2:
		pass 