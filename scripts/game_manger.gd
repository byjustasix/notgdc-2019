extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	VisualServer.set_default_clear_color(Color(0,0,0))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$cam.position = $player.position

func show_dialog(dialog):
	$ui_container/text.visible = true
	$ui_container/text.text = dialog

func hide_dialog():
	$ui_container/text.visible = false

func activate_yoku():
	# Tiles to remove (43-56)
	for i in range(43, 57):
    	$map.set_cell(i,9,-1)
	$yoku_group.start_trap()
	$yoku_group2.start_trap()
