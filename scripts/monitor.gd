extends Area2D

const GROUP = 'monitor'

var manager = null

export(int, "none", "dialog", "switch") var type
export(Array, String) var dialog = []
var dialog_index = 0
export(NodePath) var linked_trap = null
var activated = false

# Called when the node enters the scene tree for the first time.
func _ready():
	manager = get_parent().get_parent()
	if type == 1:
		anim_switch('dialog')
	elif type == 2:
		anim_switch('inactive')
	else:
		anim_switch('normal')

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func activate():
	if type == 1:
		# Advance dialog
		if dialog_index == dialog.size()-1:
			dialog_index = 0
		else:
			dialog_index += 1
		manager.show_dialog(dialog[dialog_index])
	elif type == 2:
		# Activate the monitor
		# convert to signal
		activated = true

func _on_monitor_body_entered(body):
	if body.name == 'player':
		body.action = self
		if type == 1:
			manager.show_dialog(dialog[dialog_index])
		elif type == 2:
			pass

func _on_monitor_body_exited(body):
	if body.name == 'player':
		body.action = null
		if type == 1:
			manager.hide_dialog()
			dialog_index = 0
		elif type == 2:
			pass

func anim_switch(animation):
	var new_anim = animation
	if $anim.current_animation != new_anim:
		$anim.play(animation)