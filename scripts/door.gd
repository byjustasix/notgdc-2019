extends Area2D

export var type = 0
export(Vector2) var go_to = null
export(NodePath) var connected_door = null

# Called when the node enters the scene tree for the first time.
func _ready():
	if connected_door != null:
		go_to = get_node(connected_door).position
	if go_to == null:
		go_to = position

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_door_body_entered(body):
	if body.name == 'player':
		body.action = go_to

func _on_door_body_exited(body):
	if body.name == 'player':
		body.action = null
