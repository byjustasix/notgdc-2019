extends Node2D

var yokus = null
var last_yoku = null
var current_yoku = null
var index = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	yokus = get_children()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func start_trap():
	$timer.start()

func _on_timer_timeout():
	last_yoku = current_yoku
	current_yoku = yokus[index]
	current_yoku.is_active = true
	if last_yoku != null:
		last_yoku.is_active = false
	if index != yokus.size()-1:
		index += 1
	else:
		index = 1
	
